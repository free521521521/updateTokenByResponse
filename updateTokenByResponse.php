<?php

/**
 * updateTokenByResponse
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2016-2023 Denis Chenu <http://www.sondages.pro>
 * @copyright 2016-2018 Yxplora AG <https://www.yxplora.ch/>

 * @license AGPL v3
 * @version 1.4.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 */

class updateTokenByResponse extends PluginBase
{
    protected $storage = "DbStorage";

    protected static $description = "Update token after survey is submitted";
    protected static $name = "updateTokenByResponse";

    protected $settings = [
        "information" => [
            "type" => "info",
            "class" => "alert alert-info",
            "content" =>
                "<p>Set the default value for each token, you can use Expression Manager, <strong>remind to use {} to activate expression manager</strong>. Expression is not validated, if there are error : this is not fixed. If the result of the expression is empty (trimmed) : attribute is not updated</p>",
        ],
        "em_token_firstname" => [
            "type" => "string",
            "label" => "Value for firstname",
            "default" => "",
        ],
        "em_token_lastname" => [
            "type" => "string",
            "label" => "Value for lastname",
            "default" => "",
        ],
        "em_token_email" => [
            "type" => "string",
            "label" => "Value for email",
            "default" => "",
        ],
    ];

    public function init()
    {
        $this->subscribe("afterSurveyComplete");
        $this->subscribe("beforeSurveySettings");
        if (
            version_compare(Yii::app()->getConfig("versionnumber"), "3.0.0") < 0
        ) {
            $this->subscribe("newSurveySettings");
            return;
        }
        $this->subscribe("beforeToolsMenuRender");
    }
    public function afterSurveyComplete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $iSurveyId = $this->event->get("surveyId");
        if (!tableExists("{{tokens_{$iSurveyId}}}")) {
            return;
        }
        $aTokenAttributes = $this->ownGetTokensAttributes($iSurveyId);
        if (!empty($aTokenAttributes)) {
            $aResponse = $this->pluginManager
                ->getAPI()
                ->getResponse($iSurveyId, $this->event->get("responseId"));
            if (
                $aResponse &&
                isset($aResponse["token"]) &&
                $aResponse["token"]
            ) {
                $token = $aResponse["token"];
                // OK for anonymous token : no issue
                $aAttributeUpdate = [];
                foreach ($aTokenAttributes as $attribute => $description) {
                    $sEmValue = $this->get(
                        "em_token_{$attribute}",
                        "Survey",
                        $iSurveyId,
                        ""
                    );
                    if ($sEmValue === "") {
                        $sEmValue = $this->get(
                            "em_token_{$attribute}",
                            null,
                            null,
                            ""
                        );
                    }
                    if ($sEmValue !== "") {
                        if (
                            version_compare(
                                Yii::app()->getConfig("versionnumber"),
                                "3.6.2",
                                ">="
                            )
                        ) {
                            $sValue = trim(
                                LimeExpressionManager::ProcessStepString(
                                    $sEmValue,
                                    [],
                                    3,
                                    true
                                )
                            ); // force static
                        } elseif (
                            version_compare(
                                Yii::app()->getConfig("versionnumber"),
                                "3.0",
                                ">="
                            )
                        ) {
                            $sValue = trim(
                                LimeExpressionManager::ProcessString(
                                    $sEmValue,
                                    null,
                                    [],
                                    3,
                                    1,
                                    false,
                                    false,
                                    true
                                )
                            );
                        } else {
                            $sValue = trim(
                                LimeExpressionManager::ProcessString(
                                    $sEmValue,
                                    null,
                                    [],
                                    false,
                                    3,
                                    1,
                                    false,
                                    false,
                                    true
                                )
                            );
                        }
                        if (!empty($sValue) && $sValue !== "0") {
                            // @todo : add a settings , allow empty string ?
                            $aAttributeUpdate[$attribute] = $sValue;
                        }
                    }
                }
                if (!empty($aAttributeUpdate)) {
                    $oToken = Token::model($iSurveyId)->find("token=:token", [
                        ":token" => $token,
                    ]);
                    if ($oToken) {
                        foreach ($aAttributeUpdate as $attribute => $value) {
                            $oToken->$attribute = $value;
                        }
                        $oToken->scenario = 'FinalSubmit';
                        if (!$oToken->save()) {
                            Yii::log(
                                "Error when save {$token} for survey $iSurveyId",
                                "warning",
                                "application.plugins.updateTokenByResponse"
                            );
                            Yii::log(
                                CVarDumper::dumpAsString($oToken->getErrors()),
                                "warning",
                                "application.plugins.updateTokenByResponse"
                            );
                        }
                    }
                    /* Other surveys */
                    $otherSurveys = (array) $this->get('otherSurveys', 'Survey', $iSurveyId, []);
                    foreach ($otherSurveys as $otherSurveyId) {
                        if (Survey::model()->findByPk($otherSurveyId) && Survey::model()->findByPk($otherSurveyId)->getHasTokensTable()) {
                            $oToken = Token::model($otherSurveyId)->find("token=:token", [
                                ":token" => $token,
                            ]);
                            if(!$oToken && $this->get('otherSurveyCreate', 'Survey', $iSurveyId, false)) {
                                $oToken = Token::create($otherSurveyId);
                                $oToken->token = $token;
                            }
                            if ($oToken) {
                                foreach ($aAttributeUpdate as $attribute => $value) {
                                    $oToken->$attribute = $value;
                                }
                                $oToken->scenario = 'FinalSubmit';
                                if (!$oToken->save()) {
                                    Yii::log(
                                        "Error when save {$token} for survey $iSurveyId",
                                        "warning",
                                        "application.plugins.updateTokenByResponse"
                                    );
                                }
                            }
                        } else {
                            Yii::log(
                                "Invalid survey {$otherSurveyId} for survey $iSurveyId",
                                "warning",
                                "application.plugins.updateTokenByResponse"
                            );
                        }
                    }
                }
            }
        }
    }

    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        /* save it direclty : maybe use array for setting token_ ? */
        foreach ($this->event->get("settings") as $name => $value) {
            $this->set(
                $name,
                $value,
                "Survey",
                $this->event->get("survey"),
                ""
            );
        }
    }
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        if (
            version_compare(Yii::app()->getConfig("versionnumber"), "3.0.0") < 0
        ) {
            $aSettings = $this->ownGetSurveySettings($oEvent->get("survey"));
            if (!empty($aSettings)) {
                $oEvent->set("surveysettings.{$this->id}", [
                    "name" => get_class($this),
                    "settings" => $aSettings,
                ]);
            }
            return;
        }
        $oEvent->set("surveysettings.{$this->id}", [
            "name" => get_class($this),
            "settings" => [
                "gotosettings" => [
                    "type" => "link",
                    "label" => $this->translate(
                        "Update Token by response settings"
                    ),
                    "htmlOptions" => [
                        "title" => $this->translate("This open another page"),
                    ],
                    "help" => $this->translate(
                        "This open a new page remind to save your settings."
                    ),
                    "text" => $this->translate(
                        "Edit Token by response settings"
                    ),
                    "class" => "btn btn-link",
                    "link" => Yii::app()->createUrl("admin/pluginhelper", [
                        "sa" => "sidebody",
                        "plugin" => get_class($this),
                        "method" => "actionSettings",
                        "surveyId" => $oEvent->get("survey"),
                    ]),
                ],
            ],
        ]);
    }
    /**
     * see beforeToolsMenuRender event
     *
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $surveyId = $event->get("surveyId");
        if (
            Permission::model()->hasSurveyPermission(
                $surveyId,
                "token",
                "update"
            )
        ) {
            $href = Yii::app()->createUrl("admin/pluginhelper", [
                "sa" => "sidebody",
                "plugin" => get_class($this),
                "method" => "actionSettings",
                "surveyId" => $surveyId,
            ]);
            $aMenuItem = [
                "label" => $this->translate("Token update"),
                "iconClass" => "fa fa-users", //'fa fa-edit',
                "href" => $href,
            ];
            $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
            $event->append("menuItems", [$menuItem]);
        }
    }

    /**
     * Main function
     * @param int $surveyId Survey id
     *
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(
                404,
                $this->translate("This survey does not seem to exist.")
            );
        }
        if (
            !Permission::model()->hasSurveyPermission(
                $surveyId,
                "token",
                "update"
            )
        ) {
            throw new CHttpException(401);
        }
        if (
            App()->getRequest()->getPost("save" . get_class($this))
        ) {
            $aTokenAttributes = $this->ownGetTokensAttributes($surveyId);
            foreach ($aTokenAttributes as $attribute => $label) {
                $setting = "em_token_" . $attribute;
                if (
                    !is_null(
                        App()->getRequest()->getPost($setting)
                    )
                ) {
                    $this->set($setting, App()->getRequest()->getPost($setting), "Survey", $surveyId);
                }
            }
            /* list of surveys ids after checking validity */
            $otherSurveyIds = (array) App()->getRequest()->getPost('otherSurveys');
            $otherSurveyIds = array_filter($otherSurveyIds);
            $validOtherSurveyId = array();
            if (!empty($otherSurveyIds)) {
                foreach($otherSurveyIds as $otherSurveyId) {
                    if (Permission::model()->hasSurveyPermission($otherSurveyId, 'tokens', 'update')) {
                        $validOtherSurveyId[] = $otherSurveyId;
                    }
                }
            }
            $this->set('otherSurveys', $validOtherSurveyId, "Survey", $surveyId);
            $this->set('otherSurveyCreate', App()->getRequest()->getPost('otherSurveyCreate'), "Survey", $surveyId);

            if (
                App()
                    ->getRequest()
                    ->getPost("save" . get_class($this) == "redirect")
            ) {
                Yii::app()
                    ->getController()
                    ->redirect(
                        Yii::app()->getController()->createUrl("admin/survey", [
                                "sa" => "view",
                                "surveyid" => $surveyId,
                            ])
                    );
            }

            
        }
        $aSettings = $this->ownGetSurveySettings($surveyId);
        $aData = [];
        $aData["pluginClass"] = get_class($this);
        $aData["surveyId"] = $surveyId;
        $aData["title"] = $this->translate("Update token value with response");
        $aData["warningString"] = null;
        $aData["aSettings"] = $aSettings;
        $content = $this->renderPartial("settings", $aData, true);
        return $content;
    }
    /**
     * get the token attributes array
     * @para int $iSurveyId
     * @return array
     */
    private function ownGetTokensAttributes($iSurveyId)
    {
        $aTokenAttributes = [];
        if (empty(Survey::model()->findByPk($iSurveyId))) {
            return;
        }
        if (tableExists("{{tokens_{$iSurveyId}}}")) {
            $aRealTokenAttributes = array_keys(
                Yii::app()->db->schema->getTable("{{tokens_{$iSurveyId}}}")
                    ->columns
            );
            $aRealTokenAttributes = array_combine(
                $aRealTokenAttributes,
                $aRealTokenAttributes
            );
            $aTokenAttributes = array_filter(
                Token::model($iSurveyId)->attributeLabels()
            );
            $aTokenAttributes = array_replace(
                $aRealTokenAttributes,
                $aTokenAttributes
            );
        }
        if (!tableExists("{{tokens_{$iSurveyId}}}")) {
            $aRealTokenAttributes = [
                "firstname" => gT("First name"),
                "lastname" => gT("Last name"),
                "email" => gT("Email"),
            ];
            $aUserTokenAttributes = array_filter(
                Survey::model()
                    ->findByPk($iSurveyId)
                    ->getTokenAttributes()
            );

            $aUserTokenAttributes = array_map(function ($aTokenAttribute) {
                return $aTokenAttribute["description"];
            }, $aUserTokenAttributes);
            $aTokenAttributes = array_merge(
                $aRealTokenAttributes,
                $aUserTokenAttributes
            );
        }
        $aTokenAttributes = array_diff_key($aTokenAttributes, [
            "tid" => "tid",
            "partcipant" => "partcipant",
            "participant" => "participant",
            "participant_id" => "participant_id",
            "partcipant_id" => "partcipant_id",
            "emailstatus" => "emailstatus",
            "token" => "token",
            "language" => "language",
            "blacklisted" => "blacklisted",
            "sent" => "sent",
            "remindersent" => "remindersent",
            "remindercount" => "remindercount",
            "completed" => "completed",
            "usesleft" => "usesleft",
            "validfrom" => "validfrom",
            "validuntil" => "validuntil",
            "mpid" => "mpid",
        ]);
        return $aTokenAttributes;
    }

    /**
     * update the plugin settings when loaded
     */
    public function getPluginSettings($getValues = true)
    {
        if (!Permission::model()->hasGlobalPermission("settings", "read")) {
            throw new CHttpException(403);
        }
        /* Find a better way, update via javascript ? */
        for ($cnt = 1; $cnt <= 100; $cnt++) {
            $this->settings["em_token_attribute_{$cnt}"] = [
                "type" => "string",
                "label" => "Value for attribute {$cnt}",
                "default" => "",
            ];
        }
        return parent::getPluginSettings($getValues);
    }

    /**
     * Get the survey settings
     * @param int $iSurveyId
     * @return array[]
     */
    private function ownGetSurveySettings($iSurveyId)
    {
        $aTokenAttributes = $this->ownGetTokensAttributes($iSurveyId);
        $aSettings = [];
        if (!empty($aTokenAttributes)) {
            $oSurvey = Survey::model()->findByPk($iSurveyId);
            if (!tableExists("{{tokens_{$iSurveyId}}}")) {
                $aSettings["warningNoToken"] = [
                    "type" => "info",
                    "htmlOptions" => ["class" => "alert alert-warning"],
                    "content" => $this->translate(
                        "Participant is not currently activated on this survey. This settings are used only for survey with participant."
                    ),
                ];
            }
            if ($oSurvey->anonymized == "Y") {
                $aSettings["warningAnonymized"] = [
                    "type" => "info",
                    "htmlOptions" => ["class" => "alert alert-warning"],
                    "content" => $this->translate(
                        "This survey is anonymized. This settings are used only for survey without anonymization."
                    ),
                ];
            }
            foreach ($aTokenAttributes as $attribute => $description) {
                $description =
                    trim($description) == "" ? $attribute : $description;
                $default = $this->get("em_token_{$attribute}", null, null, "");
                $aSettings["em_token_{$attribute}"] = [
                    "type" => "text",
                    "label" =>
                        "<strong>" .
                        $description .
                        "</strong> <small>" .
                        $this->translate("updated with") .
                        "</small>",
                    "current" => $this->get("em_token_{$attribute}", "Survey", $iSurveyId, ""),
                    "htmlOptions" => [
                        "rows" => 1,
                    ],
                ];
                $actualValue = $this->get(
                    "em_token_{$attribute}",
                    "Survey",
                    $iSurveyId,
                    ""
                );
                if ($actualValue) {
                    if (trim($actualValue) == "") {
                        $aSettings["em_token_{$attribute}"][
                            "help"
                        ] = $this->translate("Not updated");
                    } else {
                        $aSettings["em_token_{$attribute}"]["help"] = sprintf(
                            $this->translate("Actual value: %s"),
                            $this->ownGetHtmlExpression($actualValue, $iSurveyId)
                        );
                    }
                } elseif (!empty($default)) {
                    $aSettings["em_token_{$attribute}"]["help"] = sprintf(
                        $this->translate("Actual value (by default): %s"),
                        $this->ownGetHtmlExpression($default, $iSurveyId)
                    );
                }
            }
        }
        $aSurveyListData = $this->ownGetSurveyListData();
        unset($aSurveyListData[$iSurveyId]);
        $aSettings["otherSurveys"] = [
            'type' => 'select',
            'label' => $this->translate('Other surveys where update tokens.'),
            'help' => $this->translate('Tokens was updated only oif exist in related survey.'),
            'options' => $aSurveyListData,
            'htmlOptions' => [
                'multiple' => true,
                'placeholder' => gT("None"),
                'unselectValue' => "",
            ],
            'selectOptions' => [
                'placeholder' => gT("None"),
                ],
            'current' => $this->get('otherSurveys', 'Survey', $iSurveyId,[])
        ];
        $aSettings["otherSurveyCreate"] = [
            'type' => 'boolean',
            'label' => $this->translate('Create token if not exist in other surveys.'),
            'help' => $this->translate('If token didn\'t exist it was created with the value here, other value was not created.'),
            'current' => $this->get('otherSurveyCreate', 'Survey', $iSurveyId,0)
        ];
        return $aSettings;
    }
    /**
     * Get the complete HTML from a string with Expression for admin
     * @param string $sExpression : the string to parse
     * @param array $aReplacement : optionnal array of replacemement
     * @param boolean $forceEm : force EM or not
     *
     * @author Denis Chenu
     * @version 1.0
     */
    private function ownGetHtmlExpression(
        $sExpression,
        $iSurveyId,
        $aReplacement = [],
        $forceEm = false
    ) {
        $LEM = LimeExpressionManager::singleton();
        $aReData = [];
        if ($iSurveyId) {
            $LEM::StartSurvey($iSurveyId, "survey", [
                "hyperlinkSyntaxHighlighting" => true,
            ]); // replace QCODE
        }
        if ($forceEm) {
            $sExpression = "{" . $sExpression . "}";
        } else {
            $oFilter = new CHtmlPurifier();
            $sExpression = $oFilter->purify(
                viewHelper::filterScript($sExpression)
            );
        }
        LimeExpressionManager::ProcessString($sExpression);
        //~ templatereplace($sExpression, $aReplacement,$aReData,__CLASS__,false,null,array(),true);
        return $LEM::GetLastPrettyPrintExpression();
    }

    /**
     * Return a translated string
     * @param string
     * @return string
     */
    private function translate($string, $escapeMode = 'unescaped', $language = null)
    {
        if (method_exists($this, "gT")) {
            return $this->gT($string, $escapeMode, $language);
        }
        return $string;
    }

    /**
     * Return the survey list
     * @param string
     * @return string
     */
    private function ownGetSurveyListData()
    {
        $surveyListData = array();
        $aSurveys = Survey::model()
            ->permission(App()->user->getId())
            ->findAll([
                'select' => 'sid,language'
            ]);
        foreach ($aSurveys as $survey) {
            $surveyListData[$survey->sid] = $survey->getLocalizedTitle() . "[" . $survey->sid . "]";
        }
        return $surveyListData;
    }
}
